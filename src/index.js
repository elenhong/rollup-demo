import foo from './libs/foo.js'
import bar from './libs/bar.js'

export default {
  foo,
  bar
}