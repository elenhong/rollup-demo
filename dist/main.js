(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = global || self, global.aidol = factory());
}(this, (function () { 'use strict';

  function foo () {
    console.log('foo');
  }

  var bar = (function () {
    var _a = {
      a: 'some prop'
    },
        a = _a.a;
    console.log('bar');
    console.log(a);
  });

  var index = {
    foo: foo,
    bar: bar
  };

  return index;

})));
